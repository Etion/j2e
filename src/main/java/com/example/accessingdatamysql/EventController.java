package com.example.accessingdatamysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/event")
public class EventController {
    @Autowired
    private EventRepository eventRepository;
    
    @PostMapping(path="/add")
    public @ResponseBody String addNewEvent (
        @RequestParam String name,
        @RequestParam String date) {
            Event event = new Event();
            event.setName(name);
            event.setDate(date);
            eventRepository.save(event);
            return "Saved  <br /> <a href='http://localhost:8080/event/all'>Event list</a>";
    }
    
    @GetMapping(path="/all")
    public String getAllEvents(Model model) {
	Iterable<Event> events = eventRepository.findAll();
        model.addAttribute("events", events);
	return "eventList";
    }
    
    @GetMapping(path="register")
    public String register(){
        return "eventRegister";
    }
}
