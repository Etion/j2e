package com.example.accessingdatamysql;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path="/register")
public class RegisterController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EventRepository eventRepository;
    
    @PostMapping("/add")
    public String add(@RequestParam Integer userId,
                      @RequestParam Integer eventId){
        Optional<User> userOption = userRepository.findById(userId);
        Optional<Event> eventOption = eventRepository.findById(eventId);
        
        User user = userOption.get();
        Event event = eventOption.get();
        
        user.addEvent(event);
        event.addUser(user);
        
        userRepository.save(user);
        eventRepository.save(event);
        
        return "redirect:../user/all";
    }

    
    @GetMapping("/new")
    public String register(Model model){
        Iterable<Event> event = eventRepository.findAll();
        Iterable<User> user = userRepository.findAll();
        model.addAttribute("user", user);
        model.addAttribute("event", event);
        return "register";
    }
}
