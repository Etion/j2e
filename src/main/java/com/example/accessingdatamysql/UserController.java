package com.example.accessingdatamysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    
    @PostMapping(path="/add")
    public @ResponseBody String addNewUser (
        @RequestParam String username,
        @RequestParam String email) {
            User user = new User();
            user.setName(username);
            user.setEmail(email);
            userRepository.save(user);
            return "Saved <br /> <a href='http://localhost:8080/user/all'>User list</a>";
    }
    
    @GetMapping(path="/all")
    public String getAllUsers(Model model) {
        Iterable<User> users = userRepository.findAll();
        model.addAttribute("users", users);
	return "userList";
    }
    
    @GetMapping(path="/register")
    public String register(){
        return "userRegister";
    }
}
