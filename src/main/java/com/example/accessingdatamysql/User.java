package com.example.accessingdatamysql;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity // This tells Hibernate to make a table out of this class
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String name;
	private String email;
        
        @ManyToMany
        private List<Event> events = new ArrayList<Event>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
        
        public void addEvent(Event event){
            if(!(this.events.contains(event))){
                this.events.add(event);
            }
        }
        
        public List<Event> getEvent(){
            return this.events;
        }
        
        public String getEventString(){
            String result = "";
            for(int i=0; i< this.events.size(); i++){
                result += this.events.get(i).getName() + ", ";
            }
            if(result.length() > 2){
                result = result.substring(0, result.length()-2);
            }
            return result;
        }

}
