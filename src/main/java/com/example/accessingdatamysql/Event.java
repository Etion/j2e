/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.accessingdatamysql;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author formation
 */
@Entity
public class Event {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String name;
    private String date;
    
    @ManyToMany
    private List<User> users = new ArrayList<User>();
    
    public Integer getId(){
        return this.id;
    }
    
    public void setId(Integer id){
        this.id = id;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getDate(){
        return this.date;
    }
    
    public void setDate(String date){
        this.date = date;
    }
    
    public void addUser(User user){
        if(!(this.users.contains(user))){
            this.users.add(user);
        }
    }
    
    public List<User> getUser(){
        return this.users;
    }
    
    public String getUserString(){
            String result = "";
            for(int i=0; i< this.users.size(); i++){
                result += this.users.get(i).getName() + ", ";
            }
            if(result.length() > 2){
                result = result.substring(0, result.length()-2);
            }
            return result;
        }
}
