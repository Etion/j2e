# Projet J2E application Spring

Auteur : Piskorski Etienne
\
mail : etienne.psikorski@ensg.eu

Cette application offre une interface pour gérer des inscriptions d'utilisateurs, d'événements et l'inscription d'utilisateurs à des événements. Ces utilisateurs et événements sont stockés dans une base de donnée mysql.

prérequis :

Java : `sudo apt install default-jdk`
\
mysql : `sudo apt install mysql-server`

L'application fonctionne avec une base de donnée nommée `PiskorskiEtienne`, un utilisateur `springuser` avec le mot de passe `ThePassword` auquel on aura donné l'accés à la base de donnée.

`mysql`
\
`CREATE USER springuser@localhost IDENTIFIED WITH mysql_native_password BY ThePassword;`
\
`CREATE DATABASE PiskorskiEtienne`
\
`GRANT ALL ON PiskorskiEtienne.* TO springuser@localhost;`

lancer le serveur en effectuant la commande

`./mvnw spring-boot:run`

## Utilisation

Les inscriptions d'utilisateur et d'événements sont gérées par deux contrôleurs spring différents. Un troisième contrôleur sert à enregistrer la participation d'une personne à un événement.

**inscription d'utilisateur** : http://localhost:8080/user/register
\
**liste des utilisateurs inscrits et événements auxquels ils participent** : http://localhost:8080/user/all

**inscription d'événement** : http://localhost:8080/user/register
\
**liste des événements inscrits et utilisateurs participant** : http://localhost:8080/user/all

**inscription d'utilisateur à un événement** : http://localhost:8080/register/new
